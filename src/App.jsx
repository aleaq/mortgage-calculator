import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import BOA from "./assets/avo.png";
import avo from "./assets/avoup.png";

function App() {
  const storeState = useSelector((state) => state);
  const [active, setActive] = useState("15");
  const [base, setBase] = useState("");
  const [rate, setRate] = useState("");
  const [err, setError] = useState("");

  // set active value for loan year button
  const activeLoan = (val) => {
    setActive(val);
  };

  // calculate the interest rate on the mortgage
  const calcMortgage = () => {
    if ((base, rate, active)) {
      const percentageRate = rate / 100;
      const calcTotal = base * (1 + percentageRate * active);
      storeState.total = calcTotal;
    } else {
      setError("you missed something");
    }
  };

  // run the mortgage calc whenever there is a change to state value
  useEffect(() => {
    calcMortgage();
  }, [storeState.total]);

  return (
    <div className="App">
      <div className="container">
        <main>
          <header>
            <div>
              <img src={BOA} alt="avocodo logo" className="avocado-logo" />
            </div>
            <div className="header-content">
              <h1>Bank of Avocado</h1>
              <p>“So millennials can finally buy a house”</p>
            </div>
          </header>
          <div className="card-body">
            <div className="mortgage">
              <h2>Mortgage Calculator</h2>
              <div>
                <label htmlFor="principle">Principle</label>
                <input
                  type="number"
                  id="principle"
                  placeholder="ex: 5000"
                  onChange={(event) => setBase(event.target.value)}
                />
              </div>

              <div>
                <label htmlFor="interest">Interest Rate</label>
                <input
                  type="number"
                  id="interest"
                  placeholder="ex: 2.75"
                  disabled={!base}
                  onChange={(event) => setRate(event.target.value)}
                />
              </div>

              <button
                onClick={() => activeLoan("10")}
                className={`primary-btn ${active === "10" ? "active" : ""}`}
              >
                10 year loan
              </button>
              <button
                onClick={() => activeLoan("15")}
                className={`primary-btn ${active === "15" ? "active" : ""}`}
              >
                15 year loan
              </button>
              <button
                onClick={() => activeLoan("30")}
                className={`primary-btn ${active === "30" ? "active" : ""}`}
              >
                30 year loan
              </button>
              <div></div>
            </div>
            <div className="total">
              <h2>Total Amount</h2>
              <div className="total-card">
                <img src={avo} alt="avocodo" className="avocado" />
                <p>
                  <span>$</span>
                  {storeState.total}
                </p>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}

export default App;
