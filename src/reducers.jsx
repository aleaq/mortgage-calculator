const reducer = (state = { total: "0" }, action) => {
  switch (action.type) {
    case "MRGT_TOTAL":
      return {
        ...state,
        total: "",
      };
    default:
      return state;
  }
};

export default reducer;
