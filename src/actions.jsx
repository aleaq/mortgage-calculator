const setTotal = () => {
  return {
    type: "MRGT_TOTAL",
  };
};

export default {
  setTotal,
};
