import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import { Provider } from "react-redux";
import { createStore } from "redux";
import App from "./App";
import rootReducers from "./reducers";

const rootElement = document.getElementById("root");
const store = createStore(rootReducers);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);
